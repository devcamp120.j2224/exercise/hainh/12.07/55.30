package com.example.demo;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDrinkController {
    @CrossOrigin
	@GetMapping("/drinks")
	public ArrayList<MenuDrink> getComboMenu(){
		
		ArrayList<MenuDrink> menu = new ArrayList<MenuDrink>();
		MenuDrink traTac = new MenuDrink("TRATAC", "Trà Tắc", "13/07/2022 15:30", "13/07/2022 15:30", null, 150000);
		MenuDrink pepSi = new MenuDrink("PEPSI", "Pepsi", "13/07/2022 15:30", "13/07/2022 15:30", null, 200000);
		MenuDrink traSua = new MenuDrink("TRASUA", "Trà Sửa Trân Châu", "13/07/2022 15:30","13/07/2022 15:30", null, 250000);
        MenuDrink coCa = new MenuDrink("COCA", "Cocacola", "13/07/2022 15:30", "13/07/2022 15:30", null, 150000);
		MenuDrink laVie = new MenuDrink("LAVIE", "Nước Khoáng Lavie", "13/07/2022 15:30", "13/07/2022 15:30", null, 200000);
		MenuDrink fanTa = new MenuDrink("FANTA", "Fanta", "13/07/2022 15:30", "13/07/2022 15:30", null, 250000);
		
		menu.add(traTac);
		menu.add(pepSi);
		menu.add(traSua);
		menu.add(coCa);
		menu.add(laVie);
		menu.add(fanTa);

		return menu;
    }
}
