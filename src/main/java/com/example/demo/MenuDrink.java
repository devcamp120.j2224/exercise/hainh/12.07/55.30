package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MenuDrink {
    String maNuocUong;
    String tenNuocUong;
	String ngayTao  ;
	String ngayCapNhat;
	String ghiChu = null ;
	int donGia;

    // tạo biến Date để parse 
	Date NgayTao ;
    Date NgayCapNhat;

	public MenuDrink(String maNuocUong, String tenNuocUong, String ngayTao, String ngayCapNhat, String ghiChu, int donGia) {
		this.maNuocUong = maNuocUong;
		this.tenNuocUong = tenNuocUong;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
		this.ghiChu = ghiChu;
		this.donGia = donGia;
	}

	// method getter and setter
	public String getMaNuocUong() {
		return maNuocUong;
	}

	public void setMaNuocUong(String maNuocUong) {
		this.maNuocUong = maNuocUong;
	}

	public String getTenNuocUong() {
		return tenNuocUong;
	}

	public void setTenNuocUong(String tenNuocUong) {
		this.tenNuocUong = tenNuocUong;
	}

	public String getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(String ngayTao) {
		this.ngayTao = ngayTao;
	}

	public String getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(String ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public int getDonGia() {
		return donGia;
	}

	public void setDonGia(int donGia) {
		this.donGia = donGia;
	}

	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Override
	public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm" , Locale.ENGLISH);
        SimpleDateFormat s2 = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
       
        // dùng parse bắt buột fai có câu điều kiện try catch để hứng 
        // ví dụ ngày tháng : 13/07/2022 15:30 thì sẽ ra : 13072022153000000
        try {
            Date dateNgayTao = formatter.parse(ngayTao);
            Date dateNgayCapNhat = formatter.parse(ngayCapNhat);

            NgayTao = dateNgayTao ;
            NgayCapNhat = dateNgayCapNhat;
        } catch (ParseException e) {
           
            e.printStackTrace();
        }

        String formatNgayTao = s2.format(NgayTao);
        String formatNgayCapNhat = s2.format(NgayCapNhat);

        long resulNgayTao = Long.parseLong(formatNgayTao);
        long resulNgayCapNhat = Long.parseLong(formatNgayCapNhat);

                return "{maNuocUong : " + this.maNuocUong + 
                ", tenNuocUong" + this.tenNuocUong + 
                ", ngayTao : " +  resulNgayTao + 
                ", ngayCapNhat : " + resulNgayCapNhat + 
                ", ghiChu : " + this.ghiChu + 
                ", donGia : " + this.donGia + "}";

        }
    }
